# Rubrica JS

## Development
- `npx http-server .`

## Server
Il server con il DB si trova su [GitLab](https://gitlab.com/fondazione-its-volta/2019/programmazione-web/server).

Scarica il codice ed eseguilo con `npm start` (ricordati di eseguire `npm install` per installare le dipendenze).

Sarà disponibile all'indirizzo `http://localhost:3030/`.
