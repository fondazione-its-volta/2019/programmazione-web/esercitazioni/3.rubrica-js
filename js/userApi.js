const userApiBaseUrl = 'http://localhost:3030/api/users';
const userApiJsonHeaders = {
    'Accept': 'application/json',
    'Content-Type': 'application/json'
}

async function getUserByIdFromApi(userId) {
    try {
        const response = await fetch(`${userApiBaseUrl}/${userId}`)

        if (!response.ok) {
            return manageHttpErrorResponse(response.statusText);
        }
        return response.json();
    } catch (e) {
        alert("Il server è offline");
    }
}

async function getUsersFromApi() {
    try {
        const response = await fetch(userApiBaseUrl)

        if (!response.ok) {
            return manageHttpErrorResponse(response.statusText);
        }
        return response.json();
    } catch (e) {
        alert("Il server è offline");
    }
}


async function searchUsersFromApi(searchString) {
    try {
        const response = await fetch(`${userApiBaseUrl}-filter?key=surname&value=${searchString}`)

        if (!response.ok) {
            return manageHttpErrorResponse(response.statusText);
        }
        return response.json();
    } catch (e) {
        alert("Il server è offline");
    }
}

async function createUserFromApi(newUser) {
    try {
        const response = await fetch(userApiBaseUrl, {
            method: 'POST',
            headers: userApiJsonHeaders,
            body: JSON.stringify(newUser)
        })

        if (!response.ok) {
            return manageHttpErrorResponse(response.statusText);
        }
        return response.json();
    } catch (e) {
        alert("Il server è offline");
    }
}

async function updateUserFromApi(newUser) {
    try {
        const response = await fetch(`${userApiBaseUrl}/${newUser.id}`, {
            method: 'PUT',
            headers: userApiJsonHeaders,
            body: JSON.stringify(newUser)
        })

        if (!response.ok) {
            return manageHttpErrorResponse(response.statusText);
        }
        return response.json();
    } catch (e) {
        alert("Il server è offline");
    }
}

async function deleteUserFromApi(userId) {
    try {
        const response = await fetch(`${userApiBaseUrl}/${userId}`, {
            method: 'DELETE',
            headers: userApiJsonHeaders
        })

        if (!response.ok) {
            return manageHttpErrorResponse(response.statusText);
        }
        return response.json();
    } catch (e) {
        alert("Il server è offline");
    }
}


/**
 * Funzione per gestire gli errori
 */
function manageHttpErrorResponse(e) {
    console.log(e);
    alert(`${e}\nGuarda in console per avere maggiori informazioni.`);
}

